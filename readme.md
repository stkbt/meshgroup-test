# README #

## Assessment Test for Meshgroup ##

### Preview Online ###

[http://meshgrouptest.stkbt.ru](http://meshgrouptest.stkbt.ru)


### Setup ###

* Clone repository `git clone git@bitbucket.org:stkbt/meshgroup-test.git`
* Copy .env.example to .env `cp .env.example .env`
* Generate Laravel key `php artisan key:generate` 
* Configure database connection in .env 
* Execute `composer install` 
* execute: `php artisan migrate`
* execute `php artisan db:seed`
* serve `php -S localhost:8000 -t public`


### Screenshot ###

![alt text](/screenshot.jpg)