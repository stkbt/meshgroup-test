<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    public function scopeOfCategory($query, $category_id) 
    {

    	return $query->where('category_id', $category_id);
    }

    public function category() 
    {

    	return $this->belongsTo(\App\Category::class);
    }
}
