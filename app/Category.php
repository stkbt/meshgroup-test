<?php

namespace App;

class Category extends \Baum\Node
{
	protected $leftColumnName = 'lft';
	protected $rightColumnName = 'rgt';
	protected $depthColumnName = 'depth';
    
    public function products() 
    {

    	return $this->hasMany(\App\Product::class);
    }
}
