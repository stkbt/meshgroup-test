<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        factory(\App\Category::class, rand(4, 8))	//create roots categories
        	->create()
        	->each(function($category) {

        		$category
        			->children()	//1st level children
        			->saveMany(
        				factory(\App\Category::class, rand(0, 4))
        					->create()
        					->each(function($subcategory) {

        						$subcategory
        							->children()	//2nd level children
        							->saveMany(
        								factory(\App\Category::class, rand(0, 4))->make()
        							);
        					})
        			);
        	});
    }
}
