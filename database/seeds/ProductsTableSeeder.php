<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \App\Category::root()->get()
        	->each(function($root) {

        		$root->getLeaves()		// put products to leaves categories
		        	->each(function($category){

		        		$category->products()->saveMany(
		        			factory(\App\Product::class, rand(0, 8))->create()
		        		);
		        	});

		    });
    }
}
