<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->words(rand(2,4), true)),
        'description' => $faker->sentences(rand(1,4), true),
        'image' => $faker->imageUrl
    ];
});
