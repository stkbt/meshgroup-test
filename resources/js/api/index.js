import axios from 'axios';

export function getCategories() {


	return new Promise((resolve, reject)=>{

		axios.get('/api/categories')
			.then((response)=>{

				resolve(response.data);
			})
			.catch((error)=>{

				reject(error);
			});
	});
}

export function getProducts(category_id = null) {


	return new Promise((resolve, reject)=>{

		axios.get('/api/products', {params: {category_id}})
			.then((response)=>{

				resolve(response.data.data);
			})
			.catch((error)=>{

				reject(error);
			});
	});
}