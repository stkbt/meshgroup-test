import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);


/* 
	Pages Components
	We can load components async
 */

import Welcome from './components/Welcome';
import ProductList from './components/ProductList';


export default new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
        	path: '/', 
        	component: Welcome
        },
        {
        	path: '/category/:id', 
        	name: 'category',
        	component: ProductList
        }
    ]
});