<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Meshgroup –– Assessment</title>
        <link rel="stylesheet" href="{{ mix('assets/app.css') }}" />
    </head>
    <body>

        <div id="app"></div>

        <script src="{{ mix('assets/app.js') }}"></script>

        <script src="https://kit.fontawesome.com/25aa865a78.js" crossorigin="anonymous"></script>

    </body>
</html>