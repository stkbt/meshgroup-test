<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('categories', 'CategoryController')
	->only([
	    'index', 'show'
	]);

Route::resource('products', 'ProductController')
	->only([
	    'index', 'show'
	]);


/* Admin route group */

Route::group(['middleware' => 'auth'], function() {		

	Route::resource('categories', 'CategoryController')
		->except([
		    'index', 'show'
		]);

	Route::resource('products', 'ProductController')
		->except([
		    'index', 'show'
		]);
});
